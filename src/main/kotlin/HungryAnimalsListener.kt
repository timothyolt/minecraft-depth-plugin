package com.timothyolt.depth

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityChangeBlockEvent

class HungryAnimalsListener : Listener {
    @EventHandler
    fun onSheepEatGrass(event: EntityChangeBlockEvent) {
        if (isExternalSheepEatingGrass(event)) {
            event.isCancelled = true
        }
    }

    private fun isExternalSheepEatingGrass(event: EntityChangeBlockEvent): Boolean {
        return if (event.entityType == EntityType.SHEEP) {
            if (event.block.type !in listOf(
                    Material.GRASS,
                    Material.GRASS_BLOCK
            )) {
                Bukkit.getLogger().warning("Sheep eating: ${event.block.type}")
            }
            event !is HungryAnimalsBlockEatEvent
        } else {
            false
        }
    }
}
