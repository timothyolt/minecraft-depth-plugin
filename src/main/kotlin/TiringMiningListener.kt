package com.timothyolt.depth

import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent

class TiringMiningListener : Listener {

    @EventHandler
    fun onBlockBreakEvent(event: BlockBreakEvent) {
        val player = event.player
        val block = event.block
        val (activeItem, destroySpeed) = listOf(
            player.inventory.itemInMainHand,
            player.inventory.itemInOffHand,
        ).map { item ->
            item to block.getDestroySpeed(item, true)
        }.maxByOrNull {
            it.second
        }!!
        val type = block.type
        val name = player.name
        if (!type.isBlock) {
            Bukkit.getLogger().warning("player $name: mined $type with $activeItem (not a block)")
            return
        }
        player.saturation -= 0.15f * block.type.hardness / destroySpeed
        while (player.saturation <= -1) {
            player.saturation +=1
            if (player.foodLevel > 0) {
                player.foodLevel -= 1
            }
        }
    }
}