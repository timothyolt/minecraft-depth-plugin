package com.timothyolt.depth

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.Particle
import org.bukkit.Sound
import org.bukkit.attribute.Attribute
import org.bukkit.block.Block
import org.bukkit.entity.Animals
import org.bukkit.entity.Chicken
import org.bukkit.entity.Cow
import org.bukkit.entity.EntityType
import org.bukkit.entity.LivingEntity
import org.bukkit.entity.Pig
import org.bukkit.entity.Player
import org.bukkit.entity.Sheep
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.persistence.PersistentDataType
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitTask
import org.bukkit.util.Vector
import java.util.function.Consumer
import kotlin.math.min
import kotlin.math.roundToLong
import kotlin.random.Random

private const val starveEatAttempts = 6

class HungryAnimalsScheduler(private val plugin: Plugin) {

    fun schedule() {
        plugin.everyTick {
            val animals = getHungryAnimals().filter { it.isTicking }
            for (animal in animals) {
                val sectionCenter = animal.location.sectionCenter
                val nearbyPlayers = sectionCenter.getNearbyPlayersByDistance(128.0)
                if (nearbyPlayers.isNotEmpty()) {
                    tryToEat(animal)
                }
            }
        }
        Bukkit.getPluginManager().registerEvents(object : Listener {
            @EventHandler
            fun feedAnimals(event: PlayerInteractEntityEvent) {
                val animal = event.rightClicked as? Animals
                if (animal != null) {
                    val inventory = event.player.inventory
                    val item = inventory.getItem(event.hand)
                    if (!animal.canBreed() && item != null && animal.isBreedItem(item)) {
                        if (animal.isAdult) {
//                            inventory.setItem(event.hand, item.subtract(1))
                            item.subtract(1)
                        }
                        animal.eatAttempts = -starveEatAttempts
                        if (animal.heal(2.0)) {
                            animal.spawnHealingParticles()
                        }
                    }
                }
            }
        }, plugin)
    }

    // TODO: call event?
    private fun tryToEat(animal: Animals) {
        if (randomChance(eatChancePerTick(animal))) {
            lookDownFor2Seconds(animal)
            val adjacentGrass = animal.location.search(grazables)
            if (adjacentGrass != null) {
                val newType = if (adjacentGrass.type == Material.GRASS_BLOCK) {
                    Material.DIRT
                } else {
                    Material.AIR
                }
                adjacentGrass.setType(newType, true)
                animal.world.playSound(animal.location, Sound.BLOCK_GRASS_BREAK, 0.7f, 0.8f)
                nourish(animal)
            } else {
                starve(animal)
            }
        }
    }

    private fun lookDownFor2Seconds(animal: Animals) {
        var lookTicks = 0
        plugin.everyTick {
            animal.lookAt(animal.location.clone() + Vector(0.0, -1.0, 0.0))
            if (lookTicks > 40) {
                it.cancel()
            }
            lookTicks++
        }
    }

    private fun nourish(animal: Animals) {
        animal.eatAttempts = 0
        if (animal.heal(1.0)) {
            animal.spawnHealingParticles()
        }
    }

    private fun Animals.spawnHealingParticles() {
        world.spawnParticle(Particle.VILLAGER_HAPPY, location, 4, 1.0, 1.0, 1.0)
    }

    private fun starve(animal: Animals) {

        val newEatAttempts = animal.eatAttempts + 1
        animal.eatAttempts = if (newEatAttempts >= starveEatAttempts) {
            if (animal.name == "Phillip's") {
                val location = animal.location
                val sectionCenter = location.sectionCenter
                val nearbyPlayers = sectionCenter.getNearbyPlayersByDistance(128.0)
                val issue = if (animal.health <= 1) {
                    "WOULD HAVE DIED"
                } else {
                    animal.damage(1.0)
                    "Starving"
                }
                val playerLocations = nearbyPlayers.joinToString { it.locationString() }
                debug("${animal.name} $issue at ${location.toVector()}, section ${sectionCenter.toVector()}, nearby: $playerLocations")

            } else {
                if (animal.customName != null) {
                    val location = animal.location
                    val sectionCenter = location.sectionCenter
                    val nearbyPlayers = sectionCenter.getNearbyPlayersByDistance(128.0)
                    val playerLocations = nearbyPlayers.joinToString { it.locationString() }
                    debug("${animal.name} starving at ${location.toVector()}, section ${sectionCenter.toVector()}, nearby: $playerLocations")
                }
                animal.damage(1.0)
            }
            if (animal.isDead) {
                debug("dead ${animal.name}")
            } else if (!animal.isAdult) {
                animal.age -= eatChancePerTick(animal)
            }
            0
        } else {
            newEatAttempts
        }
    }

    private fun eatChancePerTick(animal: Animals) = when (animal.type) {
        EntityType.CHICKEN -> 1000
        EntityType.PIG -> 800
        EntityType.SHEEP -> 600
        EntityType.COW -> 400
        else -> 1000
    }

    @Suppress("UNCHECKED_CAST")
    private fun getHungryAnimals() = Bukkit.getWorlds().flatMap {
        it.getEntitiesByClasses(
            Cow::class.java,
            Sheep::class.java,
            Pig::class.java,
            Chicken::class.java,
//            Rabbit::class.java
        ) as Collection<Animals>
    }

    private val failedEatKey = NamespacedKey(plugin, "failedEatAttempts")

    private var Animals.eatAttempts: Int
    get() = persistentDataContainer.get(failedEatKey, PersistentDataType.INTEGER) ?: 0
    set(value) = persistentDataContainer.set(failedEatKey, PersistentDataType.INTEGER, value)

    private val grazables = listOf(
        Material.GRASS_BLOCK,
        Material.GRASS,
        Material.FERN,
        Material.LARGE_FERN,
        Material.TALL_GRASS,
    )
}

private fun LivingEntity.heal(value: Double): Boolean {
    val initialHealth = health
    health = min(initialHealth + value, getAttribute(Attribute.GENERIC_MAX_HEALTH)!!.value)
    return health != initialHealth
}

private operator fun Location.plus(vector: Vector) = add(vector)

private fun Location.search(types: Iterable<Material>): Block? {
    return listOf(
        block.getRelative(0, 0, 0),
        block.getRelative(-1, 0, 0),
        block.getRelative(-1, 0, -1),
        block.getRelative(0, 0, -1),
        block.getRelative(1, 0, 0),
        block.getRelative(1, 0, 1),
        block.getRelative(1, 0, 1),

        block.getRelative(0, -1, 0),
        block.getRelative(-1, -1, 0),
        block.getRelative(-1, -1, -1),
        block.getRelative(0, -1, -1),
        block.getRelative(1, -1, 0),
        block.getRelative(1, -1, 1),
        block.getRelative(1, -1, 1),
    ).filter {
        it.type in types && it.getRelative(0, 1, 0).boundingBox.volume < 1
    }.randomOrNull()
}

private fun Plugin.everyTick(action: (BukkitTask) -> Unit) =
    Bukkit.getScheduler().runTaskTimer(this, Consumer { action(it) }, 0L, 1L)

private fun randomChance(chance: Int) = Random.nextInt() % chance == 0

private val Location.sectionCenter get() = Location(
    world,
    ((x / 16).roundToLong() * 16.0) + 8.0,
    ((y / 16).roundToLong() * 16.0) + 8.0,
    ((z / 16).roundToLong() * 16.0) + 8.0
)

private fun Location.getNearbyPlayersByDistance(radius: Double): List<Player> =
    getNearbyPlayers(radius) // bounding box
    .filter { it.location.distance(this) <= radius } // rounds off the box with distance formula

private fun Player.locationString() = "$name @ ${location.toVector()}"
