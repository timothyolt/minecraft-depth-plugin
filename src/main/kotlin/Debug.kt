package com.timothyolt.depth

import org.bukkit.Bukkit

fun debug(message: String) = Bukkit.getLogger().warning(message)