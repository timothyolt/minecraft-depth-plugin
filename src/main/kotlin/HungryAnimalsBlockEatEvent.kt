package com.timothyolt.depth

import org.bukkit.block.Block
import org.bukkit.block.data.BlockData
import org.bukkit.entity.Entity
import org.bukkit.event.entity.EntityChangeBlockEvent

class HungryAnimalsBlockEatEvent(
    what: Entity,
    block: Block,
    to: BlockData
) : EntityChangeBlockEvent(what, block, to)