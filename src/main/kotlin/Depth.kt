package com.timothyolt.depth

import io.papermc.paper.event.entity.EntityMoveEvent
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.entity.LivingEntity
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.metadata.Metadatable
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask
import org.bukkit.util.BoundingBox
import org.bukkit.util.Vector

class Depth : JavaPlugin(), Listener {

    override fun onEnable() {
        Bukkit.getPluginManager().registerEvents(TiringMiningListener(), this)
        Bukkit.getPluginManager().registerEvents(HungryAnimalsListener(), this)
        HungryAnimalsScheduler(this).schedule()
//        Bukkit.getPluginManager().registerEvents(YCollisionListener(this), this)
    }

}

fun Location.nearDarkSpear57() =
    (Bukkit.getPlayer("DarkSpear57")?.location?.distance(this) ?: Double.MAX_VALUE) < 200.0

fun Location.inBermudaRectanle() =
    BoundingBox(9_500.0, -64.0, 9_500.0, 10_500.0, 350.0, 10_500.0).contains(toVector())


class YCollisionListener(private val plugin: Plugin) : Listener {
    @EventHandler
    fun stopY(event: EntityMoveEvent) {
        if (event.to.nearDarkSpear57() && event.to.inBermudaRectanle()) {
            stopCollide(event.entity, event.from, event.to) { location -> event.to = location }
        }
    }

    @EventHandler
    fun stopY(event: PlayerMoveEvent) {
        if (event.to.nearDarkSpear57() && event.to.inBermudaRectanle()) {
            stopCollide(event.player, event.from, event.to) { location ->
                event.to = location
                event.isCancelled = true
            }
        }
    }

    private fun stopCollide(me: LivingEntity, from: Location, to: Location, setTo: (Location) -> Unit) {
        val yDiff = from.y - to.y
        if (hasYCollisions(me, yDiff)) {
            setTo(to.clone().apply {
                y = from.y
            })
            applyFallDamage(me, yDiff)
            me.setGravity(false)
            me.velocity = me.velocity.clone().apply { y = 0.0 }
            startCheckingForOthersMoving(me)
        } else {
            me.setGravity(true)
        }
    }

    private fun applyFallDamage(me: LivingEntity, yDiff: Double) {
        val resetFalLDistance = me.fallDistance - yDiff
        if (resetFalLDistance > 3) {
            me.damage(resetFalLDistance - 3.0)
        }
        me.fallDistance = 0f
    }

    private fun startCheckingForOthersMoving(me: LivingEntity) {
        if (me.getGravityTask(plugin) == null) {
            val gravityTask = Bukkit.getScheduler().runTaskTimer(plugin, Runnable {
                if (!hasYCollisions(me, 0.0)) {
                    me.setGravity(true)
                    me.getGravityTask(plugin)?.cancel()
                    me.removeGravityTask(plugin)
                }
            }, 1L, 1L)
            me.setGravityTask(plugin, gravityTask)
        }
    }

    private fun hasYCollisions(me: LivingEntity, yDiff: Double): Boolean {
        val nearbyEntities = me.world
            .getNearbyEntities(me.boundingBox.clone().expand(6.0))
            .mapNotNull { other -> other as? LivingEntity }
        val fromBounds = me.boundingBox.clone().shift(0.0, yDiff, 0.0)
        return nearbyEntities.any { other ->
            other != me
                    && other.isCollidable
                    && !other.isDead
                    && other.boundingBox.overlaps(me.boundingBox)
//                    && !other.boundingBox.overlaps(fromBounds)
                    && if (yDiff > 0) other.boundingBox.minY < me.boundingBox.minY
                       else other.boundingBox.minY > me.boundingBox.minY
        }
    }
}

private const val GRAVITY_TASK = "gravityTask"

fun Metadatable.setGravityTask(plugin: Plugin, task: BukkitTask) =
    setMetadata(GRAVITY_TASK, FixedMetadataValue(plugin, task))

fun Metadatable.getGravityTask(plugin: Plugin) =
    getMetadata(GRAVITY_TASK).firstOrNull { it.owningPlugin == plugin }?.let { it.value() as BukkitTask }

fun Metadatable.removeGravityTask(plugin: Plugin) = removeMetadata(GRAVITY_TASK, plugin)
