import java.io.ByteArrayOutputStream

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.1.0"
}

group = "com.timothyolt"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven {
        name = "papermc-repo"
        url = uri("https://papermc.io/repo/repository/maven-public/")
    }
    maven {
        name = "sonatype"
        url = uri("https://oss.sonatype.org/content/groups/public/")
    }
}

dependencies {
    compileOnly("io.papermc.paper:paper-api:1.18-R0.1-SNAPSHOT")
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:1.6.10")
}

val targetJavaVersion = 17
java {
    val javaVersion = JavaVersion.toVersion(targetJavaVersion)
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
    if (JavaVersion.current() < javaVersion) {
        toolchain.languageVersion.value(JavaLanguageVersion.of(targetJavaVersion))
    }
}

tasks.withType(JavaCompile::class).configureEach {
    if (targetJavaVersion >= 10 || JavaVersion.current().isJava10Compatible()) {
        options.release.value(targetJavaVersion)
    }
}

tasks.named("processResources", ProcessResources::class) {
//    inputs.properties.put("version", version)
    filteringCharset = "UTF-8"
    filesMatching("plugin.yml") {
        expand(mapOf("version" to version))
    }
}

tasks.register("gameHost") {
    doLast {
        val host = ByteArrayOutputStream()
        exec {
            standardOutput = host
            commandLine("gcloud", "compute", "instances", "describe", "instance-1", "--zone=us-east1-b",
                "--format=get(networkInterfaces[0].accessConfigs[0].natIP)")
        }
        val trimHost = host.toString().trim()
        require(trimHost.isNotEmpty()) { "Server offline!" }
        extra.set("host", trimHost)
    }
}

tasks.register("deploy") {
    val shadowJar = tasks.named("shadowJar")
    val gameHost = tasks.named("gameHost")
    dependsOn(shadowJar, gameHost)
    doLast {
        val files = shadowJar.get().outputs.files
        val host = gameHost.get().extra.get("host")
        exec {
            val outputs = files.map { it.path }.toTypedArray()
            commandLine("scp", "-o StrictHostKeyChecking=accept-new", *outputs, "timothyolt@$host:/home/timothyolt")
        }
        exec {
            val fileNames = files.joinToString(" ") { it.name }
            commandLine("ssh", "timothyolt@$host", "-o StrictHostKeyChecking=accept-new",
                "sudo su root -c \"mv $fileNames /home/minecraft/server/plugins/\"")
        }
    }
}

tasks.register("reloadGame") {
    val gameHost = tasks.named("gameHost")
    dependsOn(gameHost)
    doLast {
        val host = gameHost.get().extra.get("host")
        exec {
            commandLine("ssh", "timothyolt@$host", "-o StrictHostKeyChecking=accept-new",
                "sudo su minecraft -c \"screen -S Minecraft -p 0 -X stuff \\\"reload confirm^M\\\"\"")
        }
    }
}

tasks.register("gameShell") {
    val gameHost = tasks.named("gameHost")
    dependsOn(gameHost)
    doLast {
        val host = gameHost.get().extra.get("host")
        exec {
            standardInput = System.`in`
            commandLine("ssh", "timothyolt@$host")
        }
    }
}
